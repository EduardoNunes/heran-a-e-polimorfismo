#ifndef HEXAGON_HPP
#define HEXAGON_HPP

#include "formageometrica.hpp"

class Hexagon : public FormaGeometrica {
    private:
        float sides[6];

    public:
        Hexagon();
        Hexagon(string type, float side);
        ~Hexagon();
        float calcula_area();
        float calcula_perimetro();

        void set_sides(float side);
        float get_sides();
};

#endif