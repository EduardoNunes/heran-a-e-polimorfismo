#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "formageometrica.hpp"

class Triangle : public FormaGeometrica {
    public:
        Triangle();
        Triangle(string type, float base, float height);
        ~Triangle();
        
        float calcula_area();  
        float calcula_perimetro();
};

#endif