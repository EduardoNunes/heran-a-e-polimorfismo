#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "formageometrica.hpp"

class Square : public FormaGeometrica{
    public:
        Square();
        Square(string type, float base, float height);
        ~Square();
};

#endif
