#ifndef PARALLELOGRAM_HPP
#define PARALLELOGRAM_HPP

#include "formageometrica.hpp"

class Parallelogram : public FormaGeometrica{
    public:
        Parallelogram();
        Parallelogram(string type, float base, float height);
        ~Parallelogram();
        float calcula_perimetro();
};

#endif