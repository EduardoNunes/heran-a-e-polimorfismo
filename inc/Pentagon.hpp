#ifndef PENTAGON_HPP
#define PENTAGON_HPP

#include "formageometrica.hpp"

class Pentagon : public FormaGeometrica{
    private:
        float sides[5];

    public:
        Pentagon();
        Pentagon(string type, float side);
        ~Pentagon();
        float calcula_area();
        float calcula_perimetro();
        
        void set_sides(float side);
        float get_sides();

};

#endif