#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "formageometrica.hpp"

class Circle : public FormaGeometrica{
    private:
        float radius;
    public:
        Circle();
        Circle(string type, float radius);
        ~Circle();
        float calcula_area();
        float calcula_perimetro();

        void set_radius(float radius);
        float get_radius();
};

#endif