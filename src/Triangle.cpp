#include "Triangle.hpp"

Triangle::Triangle(){
    set_tipo("Standard Triangle");
    set_base(1.0);
    set_altura(1.0);
}

Triangle::Triangle(string type, float base, float height){
    set_tipo(type);
    set_base(base);
    set_altura(height);
}

Triangle::~Triangle(){
}

float Triangle::calcula_area(){
    return ((get_base()*get_altura())/2);
}

float Triangle::calcula_perimetro(){
    return sqrt(pow(get_base(),2)+pow(get_altura(),2))+get_base()+get_altura();
}