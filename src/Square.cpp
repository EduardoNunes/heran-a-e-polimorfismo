#include "Square.hpp"
#include <iostream>

Square::Square(){
    set_tipo("Standard Square");
    set_base(1.0);
    set_altura(1.0);
}

Square::Square(string type, float base, float height){
    set_tipo(type);
    if(base != height){
        throw(1);
    }
    else{
        set_base(base);
        set_altura(height);
    }
}

Square::~Square(){
}
