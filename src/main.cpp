#include "formageometrica.hpp"
#include "Square.hpp"
#include "Triangle.hpp"
#include "Circle.hpp"
#include "Parallelogram.hpp"
#include "Pentagon.hpp"
#include "Hexagon.hpp"
#include <iostream>
#include <vector>

int main(){
    vector<FormaGeometrica*> list;
    list.emplace_back(new Square("Square", 1.5, 1.5));
    list.emplace_back(new Triangle("Triangle", 3.0, 4.0));
    list.emplace_back(new Circle("Circle", 3.0));
    list.emplace_back(new Parallelogram("Parallelogram", 5.0, 5.0));
    list.emplace_back( new Pentagon("Pentagon", 3.0));
    list.emplace_back(new Hexagon("Hexagon", 9.0));

    for(size_t i=0; i<list.size(); i++){
        cout<<"Tipo: "<<list[i]->get_tipo()<<'\t'<<"Area: "<<list[i]->calcula_area()<<'\t'<<"Perimetro: "<<list[i]->calcula_perimetro()<<endl;
    }

    return 0;
}