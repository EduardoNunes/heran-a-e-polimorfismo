#include "Hexagon.hpp"

Hexagon::Hexagon(){
    set_tipo("Standard Hexagon");
    set_sides(6.0);
}

Hexagon::Hexagon(string type, float side){
    set_tipo(type);
    set_sides(side);
}

Hexagon::~Hexagon(){
}

float Hexagon::calcula_area(){
    return (3*sqrt(3)*pow(sides[0],2))/2;
}

float Hexagon::calcula_perimetro(){
    return accumulate(sides, sides+6, 0.0);
}

void Hexagon::set_sides(float side){
    for(int i=0; i<6; i++){
        this->sides[i] = side;
    }
}

float Hexagon::get_sides(){
    return sides[0];
}