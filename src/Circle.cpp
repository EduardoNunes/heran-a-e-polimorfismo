#include "Circle.hpp"

Circle::Circle(){
    set_tipo("Standard Circle");
    radius = 5.5;
}

Circle::Circle(string type, float radius){
    set_tipo(type);
    this->radius = radius;
}

Circle::~Circle(){
}

float Circle::calcula_area(){
    return (3.14*pow(radius, 2));
}

float Circle::calcula_perimetro(){
    return (2*3.14*radius);
}