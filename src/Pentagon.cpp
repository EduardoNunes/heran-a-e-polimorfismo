#include "Pentagon.hpp"

Pentagon::Pentagon(){
    set_tipo("Standard Pentagon");
    set_sides(5.0);
}

Pentagon::Pentagon(string type, float side){
    set_tipo(type);
    for(int i=0; i<5; i++){
        sides[i] = side;
    }
}

Pentagon::~Pentagon(){
}

float Pentagon::calcula_area(){
    return (5*pow(sides[0],2))/(4*sqrt(5-2*sqrt(5)));
}

float Pentagon::calcula_perimetro(){
    return accumulate(sides, sides+5, 0.0);
}

void Pentagon::set_sides(float side){
    for(int i=0; i<5; i++){
        this->sides[i] = side;
    }
}

float Pentagon::get_sides(){
    return sides[0];
}