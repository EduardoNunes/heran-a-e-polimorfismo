#include "Parallelogram.hpp"

Parallelogram::Parallelogram(){
    set_tipo("Standard Parallelogram");
    set_base(1.0);
    set_altura(1.5);
}

Parallelogram::Parallelogram(string type, float base, float height){
    set_tipo(type);
    set_base(base);
    set_altura(height);
}

Parallelogram::~Parallelogram(){
}

float Parallelogram::calcula_perimetro(){
    return (2*(get_base()+get_altura()));
}